package app.runner;

import app.model.User;
import app.service.Service;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class ApplicationRunner {

    @Bean(name = "service")
    public Service service() {
        return new Service();
    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("app");
        Service service = context.getBean("service", Service.class);
        User user = service.createUser(1, "Vladislav");
        System.out.println("User created by service.createUser is: " + user.toString());
    }
}
