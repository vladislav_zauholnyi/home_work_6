package app.service;

import app.model.User;

public class Service {
    public User createUser(long id, String name) {
        System.out.println("Service.createUser(id, name) is called");
        return new User(id, name);
    }
}
