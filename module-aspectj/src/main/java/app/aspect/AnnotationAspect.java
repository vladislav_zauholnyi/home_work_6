package app.aspect;

import app.model.User;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AnnotationAspect {
    @Pointcut("execution(* app.service.*.*(..))")
    public void logMethodPointcut() {
    }

    @Before("logMethodPointcut()")
    public void before(JoinPoint joinPoint) {
        System.out.println("before() is called by method " + joinPoint.getSignature().getName());
    }

    @AfterReturning(value = "logMethodPointcut()", returning = "user")
    public void afterReturning(JoinPoint joinPoint, User user) {
        System.out.println("afterReturning() is called by " + joinPoint.getSignature().getName());
        System.out.println("Returned value is: " + user);
    }
}
