package app.service;

import app.annotation.LogMethod;
import app.model.User;

public class Service {
    @LogMethod
    public User createUser(long id, String name) {
        System.out.println("CALL TO createUser(id, name) METHOD");
        return new User(id, name);
    }
}
