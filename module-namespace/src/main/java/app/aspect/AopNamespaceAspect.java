package app.aspect;


public class AopNamespaceAspect {
    public void before() {
        System.out.println("CALL TO before() METHOD");
    }

    public void after() {
        System.out.println("CALL TO after() METHOD");
    }
}
