package app.runner;

import app.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import app.service.Service;

public class ApplicationRunner {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        Service service = context.getBean("app/service", Service.class);

        User user = service.createUser(1, "Vladislav");
        System.out.println(user);
    }
}
